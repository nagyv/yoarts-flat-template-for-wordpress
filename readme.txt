=== Flat ===
Author: YoArts
Author URL: http://www.yoarts.com
License: GNU General Public License v3.0
License URI: http://www.gnu.org/licenses/gpl-3.0.html

== About ==
FREE Flat Design WordPress Theme For Personal Blog

DW Minion WordPress theme, Copyright (C) 2013 DesignWall
DW Minion WordPress theme is licensed under the GPL v3.0

DW Minion is built with the following resources:

Bootstrap 2.3.2 http://getbootstrap.com/2.3.2/ - Copyright: @mdo: twitter.com/mdo and @fat: twitter.com/fat

Icon Fonts: Font Awesome - http://fortawesome.github.io/Font-Awesome/ - Copyright: Dave Gandy, twitter.com/davegandy

== Changelog ==
= 1.0.0 =
* Internal Release