<?php
/**
Template Name: Kategórialista
*/
?>
<?php get_header(); ?>
	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">
      <div class="page type-page hentry">
        <h1><?php echo the_title();?></h1>
      </div>
<?php
  $categories = get_categories(array(
    'exclude'=>'1',
    'hide_empty'=>0,
    'pad_counts'=>0,
    'parent'=>0
    ) );
  foreach ($categories as $category) { 
    $img = false;
    if (function_exists('z_taxonomy_image_url')) $img = z_taxonomy_image_url($category->term_id);
    ?>
    <article class="page type-page status-publish hentry">
      <header class="entry-header">
        <h2 class="entry-title">
          <a href="<?php echo get_category_link( $category->term_id ); ?>" title="<?php echo esc_attr( sprintf( __( 'Permalink to %s', 'flat' ), $category->cat_name ) ); ?>" rel="bookmark"><?php echo $category->cat_name; ?></a>
        </h2>
        <div class="entry-meta">
          <!-- <ul class="links"> -->
            <?php 
            $subcategories = get_categories(array(
              'parent'=>$category->term_id,
              'hide_empty'=>0
              ));
            $len = count($subcategories);
            $i = 0;
            foreach ($subcategories as $subcat) {
            ?>
              <span><a href="<?php echo get_category_link($subcat->term_id);?>"><?php echo $subcat->cat_name . ' (' . $subcat->count . ')';?></a></span>
            <?php 
              if($i != $len-1) {
                ?>
                <span class="sep">&middot;</span>
                <?php
              }
              $i++;
            } ?>
          <!-- </ul> -->
        </div>
      </header>
      <?php 
      if ($img) {
        ?>
        <div class="entry-thumbnail-floated">
          <img src="<?php echo $img; ?>">
        </div>
        <?php
      }
      ?>
      <div class="entry-content <?php echo $img ? ' floated' : '' ;?>">
        <?php echo $category->category_description; ?>
        <!-- <div>
          <a href="<?php echo get_category_link( $category->term_id ); ?>" title="<?php echo esc_attr( sprintf( __( 'Permalink to %s', 'flat' ), $category->cat_name ) ); ?>" rel="bookmark"><?php printf( __( "View all posts in \"%s\"" ), $category->name ); ?></a>
          <a href="<?php echo get_category_link( $category->term_id ); ?>" title="<?php echo esc_attr( sprintf( __( 'Permalink to %s', 'flat' ), $category->cat_name ) ); ?>" rel="bookmark"><?php printf( __( "View all posts in \"%s\"" ), $category->name ); ?></a>
        </div> -->
      </div>
    </article>
  <?php } ?>
		</div>
	</div>
<?php get_footer(); ?>