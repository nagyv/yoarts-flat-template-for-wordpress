��          �   %   �      P  
   Q     \     r  	   u  	     !   �  "   �     �  *   �                 2   3     f     w     |     �     �     �     �     �     �     �  9   �     $  �  ;               /     2     @  %   N  &   t     �     �  	   �     �     �  4   �     /     C     H     N     j  	   �  	   �     �     �     �  @   �  +                                      	                                                                                      
    % Comments &larr; Older Comments ,  0 Comment 1 Comment <i class="icon-chevron-left"></i> <i class="icon-chevron-right"></i> About %s Appears in the sidebar section of the site Archives Comment navigation Comments are closed. Continue reading <span class="meta-nav">...</span> Main Widget Area Meta Navigation Menu Newer Comments &rarr; Page %1$s of %2$s Page %s Pages: Permalink to %s Post navigation Search View all posts by %s <span class="meta-nav">&rarr;</span> View all posts in "%s" Project-Id-Version: flat
POT-Creation-Date: 2013-12-21 23:56+0100
PO-Revision-Date: 2013-12-21 23:56+0100
Last-Translator: Viktor Nagy <viktor.nagy@gmail.com>
Language-Team: 
Language: hu_HU
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.6.3
X-Poedit-Basepath: .
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e
X-Poedit-SearchPath-0: .
 % megjegyzés &larr; Régebbi megjegyzések ,  0 megjegyzés 1 megjegyzés <i class=”icon-chevron-left”></i> <i class=”icon-chevron-right”></i> %s Az oldalsávban jelenik meg Archívum Haladés a megjegyzések közt Megjegyzések lezárva Olvasd tovább <span class=”meta-nav”>…</span> Fő widget terület Meta Menü Újabb megjegyzések &rarr; %2$s oldalból a %1$s. %s. oldal Oldalak:  Permalink a %s-re Bejegyzések közti navigálás Keresés "%s" minden bejegyzése <span class=”meta-nav”>&rarr;</span> Minden bejegyzés a ”%s” kategóriában 